# Mini-projet C++ 2019-2020

## Utilisation

### Clonage du projet

    git clone git@gitlab.univ-nantes.fr:E174566W/libord.git
OU
    
    git clone https://gitlab.univ-nantes.fr/E174566W/libord.git

## Documentation


## Compilation

    cd C://chemin_jusque_projet/libord/
    mkdir build
    cd build
    cmake ..

### Sur Windows
* Ouvrir une fenetre Powershell
* Des configurations sont à faire sur Microsoft Visual Studio afin de générer les solutions.

A la racine du projet lord :
extraire de l'archive dans le dossier app

Vous devriez avoir : 

avec project_folder, la racine du repo git

    project_folder/
    | - app/
        | - bin/
        | - data/
        .
        .
        .

Vous allez ensuite configurer la solution :

* Ouvrez les properties de ALL_BUILD. En haut à gauche, selectioner "All Configurations" ("Toutes les configurations") dans la drop box "Configuration:".
Vous allez ensuite rechercher les parametres suivants et leur associer la valeur à droite du ':'.


Pour ALL_BUILD :

* Properties/debugging/command (Propriétés de configuration/Débogage/Commande): $(OutDir)DiceWars.exe
* Properties/debugging/command arguments (Propriétés de configuration/Débogage/Arguments de la commande): -g $(OutDir)Gui.dll -r $(OutDir)Referee.dll -m $(OutDir)ord_map.dll -s $(OutDir)ord_strat.dll -s $(OutDir)ord_strat.dll
* Properties/debugging/working directory (Propriétés de configuration/Débogage/Répertoire de travail) : $(OutDir)..\\..\\..\

Pour tous les targets :
* Properties/Configuration properties/General/Output Directory : $(SolutionDir)..\app\bin\$(Platform)\$(Configuration)\

Pour tous les targets à compiler (ord_map et ord_strat) :
* Properties/Linker/Debugging/Generate Program Database File : $(OutDir)$(AssemblyName).pdb

### Sur Linux

    make
    ./dicewars -g ./gui.so -r ./referee.so -m ../../../build/sources/map/libord_map.so -s ../../../build/sources/strat/libord_strat.so -s ../../../build/sources/strat/libord_strat.so

## Outils utilisés

### Codebunk
Ce site web nous a permis de faire du code en commun. Lors de la progression de notre projet, nous avons commencé à utilise l'outil Liveshare qui nous permet d'avoir une vue plus globale du projet.

### Liveshare
Nous avons utilisé l'outil Liveshare proposé par Visual Studio afin de travailler sur le projet. Cela à été utile afin de faire du pair programming ainsi que de tester des fonctionnalités. En conséquence, les commits de git ne sont pas très révélateurs car ils seront au nom de l'hote.

# Conclusion
Nous aurions préféré faire ce projet en présentiel. Si cela était à refaire nous aurions mis plus de temps à faire de la conception. Nous aurions du faire des diagrammes de classes, ce qui nous aurait plus guidé sur le développement.

## Credits
* Etienne MALABOEUF
* Alper OZKAN
* Louis ROUSSEL
