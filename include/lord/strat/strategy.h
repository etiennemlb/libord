#ifndef INCLUDE_LORD_STRAT_STRATAPI_H
#define INCLUDE_LORD_STRAT_STRATAPI_H

/**
 * @file stratapi.h
 * @author F. Picarougne
 * @brief L'api de la stratégie.
 * @version 0.1
 * @date 2020-04-14
 *
 */

#include "lord/common/libtools.h"
#include "lord/common/strat.h"

#ifdef __cplusplus
extern "C"
{
#endif

    // Fonction a appeler au chargement de la librairie
    // La strategie doit compl�ter la structure SPlayerInfo
    LORD_DL_EXPORT void InitStrategy(SInfo* info);

    // Fonction a appeler au debut de chaque partie
    //	* Ne pas oublier pour l'arbitre de dupliquer toute la structure map pour chaque appel !
    // valeur de retour : contexte de la strategie (e envoyer dans chaque appel de PlayTurn)
    LORD_DL_EXPORT void* InitGame(unsigned int id, unsigned int nbPlayer, const SMap* map);

    // Fonction a appeler a chaque tour sur la strategie et tant que le retour de fonction est vrai et qu'il n'y a pas
    // d'erreur.
    //	* En cas d'erreur, retablir la carte dans l'etat initial avant le premier tour du joueur.
    // valeur de retour : booleen : 0 coups termines, 1 structure turn completee avec un nouveau coup a jouer.
    // gameTurn : tour de jeu (permet a la strategie de savoir si elle a echouee au tour precedant)
    // ctx : contexte de la strategie
    LORD_DL_EXPORT int PlayTurn(unsigned int gameTurn, void* ctx, const SGameState* state, STurn* turn);

    // Fonction a appeler a la fin de chaque partie
    // ctx : contexte de la strategie
    LORD_DL_EXPORT void EndGame(void* ctx, unsigned int idWinner);

#ifdef __cplusplus
}
#endif

#endif // INCLUDE_LORD_STRAT_STRATAPI_H