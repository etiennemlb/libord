#ifndef INCLUDE_LORD_MAP_MAPGEN_H
#define INCLUDE_LORD_MAP_MAPGEN_H

/**
 * @file mapgen.h
 * @author F. Picarougne
 * @brief L'api de génération de carte.
 * @version 0.1
 * @date 2020-04-14
 *
 */


#include "lord/common/libtools.h"
#include "lord/common/map.h"

#ifdef __cplusplus
extern "C"
{
#endif

    /**
     * @brief Fonction a appeler au chargement de la librairie
     *        Le générateur de carte doit compléter la structure SMapInfo
     *
     * @param info
     * @return LORD_DL_EXPORT InitMap
     */
    LORD_DL_EXPORT void InitMap(SInfo* info);


    /**
     * @brief Fonction a appeler pour generer une nouvelle carte
     *
     * @param int les variables r et c indiquent la taille de la carte a generer ces valeurs peuvent etre modifiees par
     * le generateur de carte
     * @param int les variables r et c indiquent la taille de la carte a generer ces valeurs peuvent etre modifiees par
     * le generateur de carte
     * @return LORD_DL_EXPORT* GenerateMap
     */
    LORD_DL_EXPORT SRegions* GenerateMap(unsigned int* r, unsigned int* c);


    /**
     * @brief Fonction a appeler pour detruire les structures allouees lors de la generation de carte.
     *
     * @param regions
     * @return LORD_DL_EXPORT FreeMap
     */
    LORD_DL_EXPORT void FreeMap(SRegions* regions);

#ifdef __cplusplus
}
#endif

#endif // INCLUDE_LORD_MAP_MAPGEN_H
