#ifndef INCLUDE_LORD_COMMON_LIBTOOLS_H
#define INCLUDE_LORD_COMMON_LIBTOOLS_H

/**
 * @file libtools.h
 * @author F. Picarougne
 * @brief
 * @version 0.1
 * @date 2020-04-14
 *
 */

#if defined(WIN32)

    #include <Windows.h>
    #define LORD_DL_HANDLE                 HINSTANCE
    #define LORD_DL_LOAD(file)             LoadLibraryA(file)
    #define LORD_DL_GETPROC(handler, func) GetProcAddress(handler, func)
    #define LORD_DL_CLOSE(handler)         FreeLibrary(handler)
    #define LORD_DL_EXPORT                 __declspec(dllexport)

#else

    #include <dlfcn.h>
    #define LORD_DL_HANDLE                 void*
    #define LORD_DL_LOAD(file)             dlopen(file, RTLD_LAZY)
    #define LORD_DL_GETPROC(handler, func) dlsym(handler, func)
    #define LORD_DL_CLOSE(handler)         dlclose(handler)
    #define LORD_DL_EXPORT

#endif // defined(WIN32)

#endif // INCLUDE_LORD_COMMON_LIBTOOLS_H