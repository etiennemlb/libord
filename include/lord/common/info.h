
#ifndef INCLUDE_LORD_COMMON_INFO_H
#define INCLUDE_LORD_COMMON_INFO_H

/**
 * @file info.h
 * @author F. Picarougne
 * @brief
 * @version 0.1
 * @date 2020-04-14
 *
 */


/**
 * @brief Structure définissant l'identification de la librairie
 * 
 */
const unsigned int NbMaxMembers = 5;
struct SInfo
{
    char name[30];                  //!< Nom de la librairie
    char members[NbMaxMembers][50]; //!< Noms des personnes participant au projet
};

/**
 * @brief Types prédéfinis pour simplifier la récupération des fonctions dans la librairie
 * 
 */
using pInitLib = void (*)(SInfo* info);

#endif // INCLUDE_LORD_COMMON_INFO_H
