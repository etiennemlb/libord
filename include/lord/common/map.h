#ifndef INCLUDE_LORD_COMMON_MAP_H
#define INCLUDE_LORD_COMMON_MAP_H

/**
 * @file map.h
 * @author F. Picarougne
 * @brief
 * @version 0.1
 * @date 2020-04-14
 *
 */


#include "lord/common/info.h"

#include <stddef.h>

const unsigned int NbMaxMapMembers = 5;


/**
 * @brief Structure définissant une cellule hexagonale
 * 
 */
struct SRegionCell
{
    unsigned int x;
    unsigned int y;
};

/**
 * @brief Structure définissant une région : ensemble de cellules hexagonales
 * 
 */
struct SRegion
{
    SRegionCell* cells;
    size_t       nbCells;
};

/**
 * @brief Ensemble de régions
 * 
 */
struct SRegions
{
    SRegion* region;
    size_t   nbRegions;
};

/**
 * @brief Types prédéfinis pour simplifier la récupération des fonctions dans la librairie
 * 
 */
using pGenerateMap = SRegions* (*)(unsigned int* r, unsigned int* c);

/**
 * @brief Types prédéfinis pour simplifier la récupération des fonctions dans la librairie
 * 
 */
using pFreeMap = void (*)(SRegions* regions);

#endif // INCLUDE_LORD_COMMON_MAP_H
