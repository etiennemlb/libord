#ifndef INCLUDE_LORD_COMMON_STRAT_H
#define INCLUDE_LORD_COMMON_STRAT_H

/**
 * @file strat.h
 * @author F. Picarougne
 * @brief
 * @version 0.1
 * @date 2020-04-14
 *
 */

#include "lord/common/info.h"


/**
 * @brief Structure définissant les caractéristiques d'une cellule
 * 
 */
struct SCellInfo
{
    unsigned int id;      //!< Id de la cellule
    unsigned int owner;   //!< Id du joueur qui la possède
    unsigned int nbDices; //!< Nombre de dés
};

struct SGameState
{
    SCellInfo*   cells; //!< Tableau d'informations sur le contenu des cellules
    unsigned int nbCells;
    unsigned int points[8];    //!< Points de chaque joueur
    unsigned int diceStock[8]; //!< Réserve de dés de chaque joueur
};

/**
 * @brief Structure définissant les caractéristiques d'une cellule
 * 
 */
struct SCell;
using pSCell = SCell*;
struct SCell
{
    SCellInfo    infos;       //!< Informations sur la cellule
    pSCell*      neighbors;   //!< Tableau de pointeurs vers des cellules voisines
    unsigned int nbNeighbors; //!< Nombre de cellules voisines
};

/**
 * @brief Structure définissant la carte globale du jeu
 * 
 */
struct SMap
{
    SCell*       cells;   //!< Tableau des cellules
    unsigned int nbCells; //!< Nombre de cellules
};

/**
 * @brief Structure définissant les paramètres d'un coup joué
 * 
 */
struct STurn
{
    unsigned int cellFrom;
    unsigned int cellTo;
};

/**
 * @brief Structure définissant un combat
 *
 */
struct Fight
{
    int   Me;
    int   Opponent;
    float Ratio;
};

/**
 * @brief Types prédéfinis pour simplifier la récupération des fonctions dans la librairie
 * 
 */
using pInitGame = void* (*)(unsigned int id, unsigned int nbPlayer, const SMap* map);
/**
 * @brief Types prédéfinis pour simplifier la récupération des fonctions dans la librairie
 * 
 */
using pPlayTurn = int (*)(unsigned int gameTurn, void* ctx, const SGameState* state, STurn* turn);

/**
 * @brief Types prédéfinis pour simplifier la récupération des fonctions dans la librairie
 * 
 */
using pEndGame = void (*)(void* ctx, unsigned int idWinner);
#endif // INCLUDE_LORD_COMMON_STRAT_H
