#ifndef INCLUDE_LORD_COMMON_REFEREE_H
#define INCLUDE_LORD_COMMON_REFEREE_H

/**
 * @file referee.h
 * @author F. Picarougne
 * @brief
 * @version 0.1
 * @date 2020-04-14
 *
 */

#include "lord/common/info.h"
#include "lord/map/mapgen.h"
#include "lord/strat/strategy.h"

struct IStrategy
{
    virtual ~IStrategy() {}
    virtual void InitGame(unsigned int id, unsigned int nbPlayer, const SMap* map)     = 0;
    virtual int  PlayTurn(unsigned int gameTurn, const SGameState* state, STurn* turn) = 0;
};

struct SGameTurn
{
    unsigned int idPlayer;
    STurn        turn;
    unsigned int dices[2][8];
};

struct IComReferee
{
    virtual ~IComReferee() {}
    virtual void InitGameState(unsigned int idTurn, const SGameState* state)                               = 0;
    virtual void UpdateGameState(unsigned int idTurn, const SGameTurn* turn, const SGameState* finalState) = 0;
    virtual void UpdateGameStatePostTurn(unsigned int idTurn, const SGameState* finalState)                = 0;
    virtual void EndGame(unsigned int idTurn, unsigned int idWinner)                                       = 0;
};

struct IReferee
{
    virtual ~IReferee() {}
    virtual void NewGame(IComReferee* comm, IStrategy** strategies, const SRegions* map) = 0;
};

/**
 * @brief Types prédéfinis pour simplifier la récupération des fonctions dans la librairie
 * 
 */
using pRefereeFactory = IReferee* (*)();

/**
 * @brief Types prédéfinis pour simplifier la récupération des fonctions dans la librairie
 * 
 */
using pRefereeDelete = void (*)(IReferee* obj);

#endif // INCLUDE_LORD_COMMON_REFEREE_H