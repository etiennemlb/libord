#ifndef INCLUDE_LORD_COMMON_GUI_H
#define INCLUDE_LORD_COMMON_GUI_H

/**
 * @file gui.h
 * @author F. Picarougne
 * @brief
 * @version 0.1
 * @date 2020-04-14
 *
 */

#include "info.h"
#include "referee.h"


struct IGui
{
    virtual ~IGui() {}
    virtual void InitGui(unsigned int nbPlayer, const SRegions* map)                                       = 0;
    virtual void SetPlayerInfo(unsigned int id, const SInfo* info)                                         = 0;
    virtual void WaitReady()                                                                               = 0;
    virtual void SetGameState(unsigned int idTurn, const SGameState* state)                                = 0;
    virtual void UpdateGameState(unsigned int idTurn, const SGameTurn* turn, const SGameState* finalState) = 0;
    virtual void EndGame(unsigned int idTurn, unsigned int idWinner)                                       = 0;
};

// Types prédéfinis pour simplifier la récupération des fonctions dans la librairie
using pGuiFactory = IGui* (*)();
using pGuiDelete  = void (*)(IGui* obj);


#endif // INCLUDE_LORD_COMMON_GUI_H