#ifndef SOURCES_STRAT_INCLUDE_MYSTRATEGY_H
#define SOURCES_STRAT_INCLUDE_MYSTRATEGY_H

/**
 * @file mystrat.h
 * @author F. Picarougne
 * @brief
 * @version 0.1
 * @date 2020-04-14
 *
 */

#include "lord/common/strat.h"

#include <vector>


class MyStrategy
{
public:
public:
    MyStrategy(unsigned int id, unsigned int nbPlayer, const SMap* map);
    MyStrategy(const MyStrategy& obj) = delete;
    MyStrategy(MyStrategy&& obj)      = delete;
    ~MyStrategy();
    MyStrategy& operator=(const MyStrategy& obj) = delete;
    MyStrategy& operator=(MyStrategy&& obj) = delete;

public:
    /**
    * Used to update game state
    *
    * \param state
    */
    void Update(const SGameState* state);


    /**
     * Get all the cells that belong to me
     *
     * \return
     */
    std::vector<int> GetMyCells();

    /**
    * Get all the fights availaible between "me" and ennemies
    *
    *
    * \return a list (a vector) of fights
    */
    std::vector<Fight> GetAllFight();


    /**
    *
    *
    * \param gameTurn
    * \param ctx
    * \param state
    * \param turn
    * \return 1 if a move is done : turn modified
    *         0 otherwise : no move
    */
    int PlayTurn(unsigned int gameTurn, void* ctx, const SGameState* state, STurn* turn);

protected:
    const unsigned int Id;
    const unsigned int NbPlayer;
    SMap               Map;
};

#endif // SOURCES_STRAT_INCLUDE_MYSTRATEGY_H
