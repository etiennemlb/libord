/**
 * @file strategy.cc
 * @brief
 * @version 0.1
 * @date 2020-04-14
 *
      Copyright 2020 Etienne MALABOEUF, Alper OZKAN, Louis ROUSSEL

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#include "lord/strat/strategy.h"

#include "mystrategy.h"

#include <cstring>
#include <iostream>

#ifdef _MSC_VER
    #pragma warning(disable : 4996) // disable _CRT_SECURE_NO_WARNINGS
#endif

LORD_DL_EXPORT void InitStrategy(SInfo* info)
{
    std::cout << "InitPlayer" << std::endl;

    std::strcpy(info->name, "EAL Strategy");
    std::strcpy(info->members[1], "Etienne MALABOEUF");
    std::strcpy(info->members[2], "Alper OZKAN");
    std::strcpy(info->members[3], "Louis ROUSSEL");
}

LORD_DL_EXPORT void* InitGame(unsigned int id, unsigned int nbPlayer, const SMap* map)
{
    return new MyStrategy{id, nbPlayer, map};
}

LORD_DL_EXPORT int PlayTurn(unsigned int gameTurn, void* ctx, const SGameState* state, STurn* turn)
{
    MyStrategy* mem = static_cast<MyStrategy*>(ctx);
    return mem->PlayTurn(gameTurn, ctx, state, turn);
}

LORD_DL_EXPORT void EndGame(void* ctx, unsigned int idWinner)
{
    (void)idWinner;
    MyStrategy* mem = static_cast<MyStrategy*>(ctx);
    delete mem;
}
