#include "mystrategy.h"

#include <algorithm>
#include <vector>

MyStrategy::MyStrategy(unsigned int id, unsigned int nbPlayer, const SMap* map)
    : Id(id)
    , NbPlayer(nbPlayer)
{
    // full copy of the stuct SMap inside Map

    Map.cells = new SCell[map->nbCells];

    for(size_t i = 0; i < map->nbCells; i++)
    {
        Map.cells[i].infos.id      = map->cells[i].infos.id;
        Map.cells[i].infos.owner   = map->cells[i].infos.owner;
        Map.cells[i].infos.nbDices = map->cells[i].infos.nbDices;

        Map.cells[i].nbNeighbors = map->cells[i].nbNeighbors;
    }

    for(size_t i = 0; i < map->nbCells; i++)
    {
        Map.cells[i].neighbors = new pSCell[Map.cells[i].nbNeighbors];
        for(size_t j = 0; j < Map.cells[i].nbNeighbors; j++)
        {
            Map.cells[i].neighbors[j] = &(Map.cells[map->cells[i].neighbors[j]->infos.id]);
        }
    }

    Map.nbCells = map->nbCells;
}

MyStrategy::~MyStrategy()
{
    // destroy Map struct
    delete Map.cells;
}


void MyStrategy::Update(const SGameState* state)
{
    for(size_t i = 0; i < Map.nbCells; i++)
    {
        Map.cells[i].infos.id      = state->cells[i].id;
        Map.cells[i].infos.owner   = state->cells[i].owner;
        Map.cells[i].infos.nbDices = state->cells[i].nbDices;
    }
}


std::vector<int> MyStrategy::GetMyCells()
{
    std::vector<int> id_list;
    for(unsigned int i = 0; i < Map.nbCells; ++i)
    {
        if(Id == Map.cells[i].infos.owner)
        {
            id_list.push_back(i);
        }
    }
    return id_list;
}


std::vector<Fight> MyStrategy::GetAllFight()
{
    std::vector<int>   id_my_cells = GetMyCells();
    std::vector<Fight> fights_list;

    for(size_t i = 0; i < id_my_cells.size(); i++)
    {
        SCell& me_cell = Map.cells[id_my_cells[i]];

        for(size_t j = 0; j < me_cell.nbNeighbors; j++)
        {
            SCell& neigh_cell  = *me_cell.neighbors[j];
            float  fight_ratio = (float)me_cell.infos.nbDices / neigh_cell.infos.nbDices;


            //Select opponent cells
            //Filter on :
            //            my cells that have more than 1 dice (strict)
            //            do not fight opponent with more dices / with the same ammount
            if((neigh_cell.infos.owner != Id) && (me_cell.infos.nbDices > 1) && (me_cell.infos.nbDices > neigh_cell.infos.nbDices))
            {
                Fight current_fight    = {};
                current_fight.Me       = id_my_cells[i];      //id of my cell
                current_fight.Opponent = neigh_cell.infos.id; //id of the opponent cell
                current_fight.Ratio    = fight_ratio;

                fights_list.push_back(current_fight);
            }
        }
    }
    return fights_list;
}


int MyStrategy::PlayTurn(unsigned int gameTurn, void* ctx, const SGameState* state, STurn* turn)
{
    (void)ctx;
    (void)gameTurn;

    Update(state);

    std::vector<Fight> fight_list = GetAllFight();

    if(!fight_list.empty())
    {
        //Compare fight value with ratio
        sort(fight_list.begin(), fight_list.end(), [](Fight& f1, Fight& f2) -> bool { return f1.Ratio > f2.Ratio; }); //sort of fights

        //we take the fist fight in the list
        int rand_int   = 0;
        turn->cellFrom = fight_list[rand_int].Me;
        turn->cellTo   = fight_list[rand_int].Opponent;

        return (1);
    }
    else
    {

        return (0);
    }
}
