#ifndef SOURCES_MAP_INCLUDE_STRATBASE_H
#define SOURCES_MAP_INCLUDE_STRATBASE_H

/**
 * @file stratbase.h
 * @author E. Malaboeuf, A. Ozkan, L. Roussel
 * @brief Strat�gie de G�n�ration de Map Lin�aire
 * @version 0.1
 * @date 2020-05-25
 *
 */

#include "loader.h"

#include <cstddef>
#include <cstdint>
#include <unordered_map>
#include <vector>

class StratBase
{
public:
    // public types

    typedef std::size_t size_type;
    typedef std::size_t dimension_type;
    typedef uint8_t     cell_type;

protected:
    // protected types

    using Grid = std::vector<std::vector<cell_type>>; //<! Up to 255 players

public:
    // public methods

    StratBase(Regions& regions, size_type nb_row, size_type nb_col);
    virtual ~StratBase() = default;

    //! G�n�re la carte
    void GenMap();

    //! Affiche la grille
    void PrintGrid();

protected:
    // protected methods

    //! Calcule la distance Euclidienne entre deux points.
    /*!
	  \param x1 X du point 1.
	  \param y1 Y du point 1.
	  \param x2 X du point 2.
	  \param y2 Y du point 2.
	  \return La distance euclidienne
	*/
    double EuclideanDist(size_type x1, size_type y1, size_type x2, size_type y2);

    //! La taille du voisinage de la case donn�e.
    /*!
	  \param x x du point donn�.
	  \param y y du point donn�.
	  \param neighboor pointeur vers un vecteur de vecteur ou on ajoute les voisins du point donn�.
	  \return la taille du parametre neighboor.
	*/
    size_type GetNeighbour(size_type x, size_type y, std::vector<vector2>& neighboor);

    //!
    /*! Remplit une grille vide.
	*/
    void FillEmptyGrid();

    //!
    /*! Fonction abstraite qui permet la propagation des r�gions.
	  \param region_coord_list Liste des coordonn�es de r�gions
	  \param n Taille avec laquelle la r�gion sera aggrandie
	*/
    virtual void EnlargeRegion(std::vector<vector2>& region_coord_list, size_type n) = 0;

    //!
    /*! Fonction abstraite qui permet l'initialisation des r�gions
	  \param reg_coords Vecteur avec
	  \param region_coords
	*/
    virtual void InitRegions(std::vector<vector2>& reg_coords, const vector2& region_coords) = 0;

protected:
    // data members
    Regions&  regions_; /*!< Pointeur vers un vecteur de vecteur repr�sentant les r�gions dans la grille.*/
    size_type nb_row_;  /*!< Nombre de lignes dans une grille.*/
    size_type nb_col_;  /*!< Nombre de colonnes dans une grille.*/
    Grid      grid_;
};

#endif // SOURCES_MAP_INCLUDE_STRATBASE_H