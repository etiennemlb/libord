#ifndef SOURCES_MAP_INCLUDE_DEFAULTMAP_H
#define SOURCES_MAP_INCLUDE_DEFAULTMAP_H

/**
 * @file defaultmap.h
 * @author F. Picarougne
 * @brief
 * @version 0.1
 * @date 2020-04-14
 *
 */

#include "loader.h"

void LoadDefaultMap(Regions& regions);

#endif // SOURCES_MAP_INCLUDE_DEFAULTMAP_H
