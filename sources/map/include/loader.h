#ifndef SOURCES_MAP_INCLUDE_LOADER_H
#define SOURCES_MAP_INCLUDE_LOADER_H

/**
 * @file loader.h
 * @author F. Picarougne
 * @brief
 * @version 0.1
 * @date 2020-04-14
 *
 */

#include "lord/common/map.h"

#include <utility>
#include <vector>

using MapSizeType = long;
using vector2     = std::pair<MapSizeType, MapSizeType>;
using Regions     = std::vector<std::vector<vector2>>;

SRegions* ConvertMap(Regions& regions, unsigned int& nbR, unsigned int& nbC);
void      DeleteMap(SRegions* regions);

#endif // SOURCES_MAP_INCLUDE_LOADER_H
