#ifndef SOURCES_MAP_INCLUDE_SGMO_H
#define SOURCES_MAP_INCLUDE_SGMO_H

/**
 * 
 * Stategy Generation Map Organic
 * 
 * @file sgmo.h
 * @author E. Malaboeuf, A. Ozkan, L. Roussel
 * @brief Strat�gie de G�n�ration de Map Organique
 * @version 0.1
 * @date 2020-05-27
 *
 */

#include "stratbase.h"


class SGMO : public StratBase
{
public:
    // public types

protected:
    // protected types

    using StratBase::size_type;

    using StratBase::GetNeighbour;
    using StratBase::grid_;
    using StratBase::nb_col_;
    using StratBase::nb_row_;


public:
    // public methods

    SGMO(Regions& regions, size_type nb_row, size_type nb_col);
    virtual ~SGMO() = default;

protected:
    // protected methods

    /**
	 * Used to init a region
	 *
	 * \param reg_coords
	 * \param region_coords
	 */
    void InitRegions(std::vector<vector2>& reg_coords, const vector2& region_coords) override;


    /**
	 *	Go to a region list from src using organic growth
	 *
	 * \param src source vector
	 * \param dest destination vector
	 * \param n
	 */
    void OrganicGrowth(cell_type      region_id,
                       const vector2& from,
                       size_type      n);

    /**
	 * Enlarge a region using organic method
	 *
	 * \param coords of the region
	 * \param n used to handle the enlarge size
	 */
    void EnlargeRegion(std::vector<vector2>& coords, size_type n) override;

protected:
    // data members
};

#endif // SOURCES_MAP_INCLUDE_SGMO_H