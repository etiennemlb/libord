#ifndef SOURCES_MAP_INCLUDE_SGML_H
#define SOURCES_MAP_INCLUDE_SGML_H

/**
 * 
 * Stategy Generation Map Linear
 * 
 * @file sgml.h
 * @author E. Malaboeuf, A. Ozkan, L. Roussel
 * @brief Strat�gie de G�n�ration de Map Lin�aire
 * @version 0.1
 * @date 2020-05-25
 *
 */

#include "stratbase.h"

#include <unordered_set>


class SGML : public StratBase
{
public:
    // public types

protected:
    // protected types

    struct Vector2Hash
    {
        inline std::size_t operator()(const vector2& val) const
        {
            return val.first * 31 + val.second;
        }
    };

    using Vector2Set = std::unordered_set<vector2, Vector2Hash>;

    using StratBase::size_type;

    using StratBase::EuclideanDist;
    using StratBase::GetNeighbour;
    using StratBase::grid_;
    using StratBase::nb_col_;
    using StratBase::nb_row_;

public:
    // public methods

    SGML(Regions& regions, size_type nb_row, size_type nb_col);
    virtual ~SGML() = default;

protected:
    // protected methods

    /**
     * Go to dest from src in linear shape
     * 
     * \param src source vector
     * \param dest destination vector
     * \param n
     */
    void GrowInDirection(const vector2& src, const vector2& dest, size_type n);

    /**
     * Used to init a region
     * 
     * \param reg_coords
     * \param region_coords
     */
    void InitRegions(std::vector<vector2>& reg_coords, const vector2& region_coords) override;

    /**
     * get the edge of a region from a cell of a region
     * 
     * \param ker the kernel of a region
     * \param available_tile
     */
    void GetBord(const vector2&                           ker,
                 std::vector<std::pair<double, vector2>>& available_tile);

    void __GetBord(const vector2&                           ker,
                   std::vector<std::pair<double, vector2>>& available_tile,
                   Vector2Set&                              visited_cells);

    /**
     * Enlarge a region using linear method
     * 
     * \param coords of the region
     * \param n used to handle the enlarge size
     */
    void EnlargeRegion(std::vector<vector2>& coords, size_type n) override;

protected:
    // data members
};

#endif // SOURCES_MAP_INCLUDE_SGML_H