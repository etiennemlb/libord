/**
 * @file staticmapgenerator.cc
 * @brief
 * @version 0.1
 * @date 2020-04-14
 *
      Copyright 2020 Etienne MALABOEUF, Alper OZKAN, Louis ROUSSEL

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#include "lord/map/mapgen.h"

#include "sgml.h"
#include "sgmo.h"

#include <cstring>
// #include "defaultmap.h"
#include "loader.h"


#ifdef _MSC_VER
    #pragma warning(disable : 4996) // disable _CRT_SECURE_NO_WARNINGS
#endif

LORD_DL_EXPORT void InitMap(SInfo* info)
{
    std::strcpy(info->name, "EAL Map");
    std::strcpy(info->members[0], "Etienne MALABOEUF");
    std::strcpy(info->members[1], "Alper OZKAN");
    std::strcpy(info->members[2], "Louis ROUSSEL");
}

LORD_DL_EXPORT SRegions* GenerateMap(unsigned int* r, unsigned int* c)
{
    Regions regions;
    //LoadDefaultMap(regions);

    SGMO mapgen(regions, *r, *c);
    //or
    // SGML mapgen(regions, *r, *c);
    mapgen.GenMap();

    unsigned int nbR, nbC;
    SRegions*    sregions = ConvertMap(regions, nbR, nbC);
    if(r)
        *r = nbR;
    if(c)
        *c = nbC;

    return (sregions);
}

LORD_DL_EXPORT void FreeMap(SRegions* regions)
{
    DeleteMap(regions);
}
