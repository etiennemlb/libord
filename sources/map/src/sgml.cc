#include "sgml.h"

#include <algorithm>
#include <cstdlib>

////////////////////////////////////////////////////////////////
///           public methods                                 ///
////////////////////////////////////////////////////////////////


SGML::SGML(Regions& regions, size_type nb_row, size_type nb_col)
    : StratBase{regions, nb_row, nb_col}
{
}


void SGML::GrowInDirection(const vector2& src, const vector2& dest, size_type n)
{
    std::vector<std::pair<double, std::pair<MapSizeType, MapSizeType>>> available_tiles;

    GetBord(src, available_tiles);

    for(auto& elem : available_tiles)
    {
        elem.first = EuclideanDist(elem.second.first, elem.second.second, dest.first, dest.second);
    }

    std::sort(available_tiles.begin(), available_tiles.end(), [](auto& a, auto& b) { return a.first < b.first; });

    size_type i = 0;
    while(i < 3 && i != available_tiles.size())
    {
        size_type tile_grown_count = n;

        vector2& from      = available_tiles[i].second;
        vector2  direction = {dest.first - from.first, dest.second - from.second};

        int direction_x = 0;
        int direction_y = 0;

        int sign_dir_x = 1;
        if(direction.first < 0)
        {
            sign_dir_x = -1;
        }
        int sign_dir_y = 1;
        if(direction.second < 0)
        {
            sign_dir_y = -1;
        }

        while(direction_x < abs(direction.first) && tile_grown_count)
        {
            if(grid_[from.second + sign_dir_y * direction_y][from.first + sign_dir_x * direction_x] != 0 &&
               grid_[from.second + sign_dir_y * direction_y][from.first + sign_dir_x * direction_x] != grid_[src.second][src.first])
            {
                break;
            }
            else
            {
                grid_[from.second + sign_dir_y * direction_y][from.first] = grid_[src.second][src.first];

                grid_[from.second + sign_dir_y * direction_y][from.first + sign_dir_x * direction_x] = grid_[src.second][src.first];
                tile_grown_count--;
                if(direction_y < abs(direction.second))
                {
                    direction_y++;
                }
            }

            direction_x++;
        }
        i++;
    }
}

void SGML::InitRegions(std::vector<vector2>& reg_coords, const vector2& region_coords)
{
    reg_coords.push_back(region_coords);
}

void SGML::__GetBord(
    const vector2&                           ker,
    std::vector<std::pair<double, vector2>>& available_tile,
    Vector2Set&                              visited_cells)
{
    std::vector<vector2> tempvec_neigh;

    visited_cells.insert(ker); //centre visit�

    GetNeighbour(ker.first, ker.second, tempvec_neigh); //voisins

    for(const auto& elem : tempvec_neigh) //check des voisins si != ker
    {
        if(grid_[ker.second][ker.first] != grid_[elem.second][elem.first]) //si diff => pt du bord
        {
            visited_cells.insert(elem);
            available_tile.push_back({0., ker}); // a refaire : pa bon

            break;
        }
        else
        {
            if(!(visited_cells.find(elem) != visited_cells.end())) //si pas visit�
            {
                __GetBord(elem, available_tile, visited_cells);
            }
        }
    }
}

void SGML::GetBord(
    const vector2&                           ker,
    std::vector<std::pair<double, vector2>>& available_tile)
{
    Vector2Set visited_cells;
    __GetBord(ker, available_tile, visited_cells);
}

// Fonction qui pour un entier donn� aggrandit une r�gion
void SGML::EnlargeRegion(std::vector<vector2>& coords, size_type n)
{
    for(size_type i = 0; i < coords.size(); ++i)
    {
        auto&                                   current = coords[i];
        std::vector<std::pair<double, vector2>> attraction_vectors;
        for(size_type j = 0; j < coords.size(); ++j)
        {
            if(j != i)
            {
                auto&  other = coords[j];
                double dist  = EuclideanDist(current.first, current.second, other.first, other.second);

                attraction_vectors.push_back({dist, other});
            }
        }

        std::sort(attraction_vectors.begin(), attraction_vectors.end(), [](auto& a, auto& b) { return a.first < b.first; });
        for(const auto& destination : attraction_vectors)
        {
            const auto dest_reg = destination.second;
            (void)n;
            GrowInDirection(current, dest_reg, 3); // TODO, n non utilisé ?
        }
    }
}
