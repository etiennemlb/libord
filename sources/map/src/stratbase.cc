#include "stratbase.h"

#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iostream>

#define FILLING(r, c) ((r * c) / ::pow(2, 3.75))

////////////////////////////////////////////////////////////////
///           public methods                                 ///
////////////////////////////////////////////////////////////////

StratBase::StratBase(Regions& regions, size_type nb_row, size_type nb_col)
    : regions_{regions}
    , nb_row_{nb_row}
    , nb_col_{nb_col}
    , grid_{nb_row_}
{
    FillEmptyGrid();
}

void StratBase::GenMap()
{
    ::srand((unsigned)time(NULL));

    regions_.clear();

    size_type nb_reg      = FILLING(nb_row_, nb_col_);
    size_type reg_counter = 0;

    std::vector<vector2>                             reg_coords;
    std::unordered_map<size_t, std::vector<vector2>> reg_map;

    for(size_type i = 0; i < nb_reg; ++i)
    {
        MapSizeType y = ::rand() % nb_row_;
        MapSizeType x = ::rand() % nb_col_;

        while(grid_[y][x])
        {
            y = ::rand() % nb_row_;
            x = ::rand() % nb_col_;
        }

        grid_[y][x] = ++reg_counter;

        std::vector<vector2> neigh_vec;
        GetNeighbour(x, y, neigh_vec);

        InitRegions(reg_coords, {x, y});

        for(const auto& elem : neigh_vec)
        {
            grid_[elem.second][elem.first] = reg_counter;
        }
    }

    EnlargeRegion(reg_coords, ::pow(nb_reg, 0.5));

    for(size_type i = 0; i < nb_col_; ++i)
    {
        for(size_type j = 0; j < nb_row_; ++j)
        {
            if(grid_[j][i] != 0)
            {
                reg_map[grid_[j][i]].push_back({j, i});
            }
        }
    }

    for(auto& elem : reg_map)
    {
        regions_.push_back(elem.second);
    }
}

void StratBase::PrintGrid()
{
    bool spacer = false; // permet de d�caler dans le texte
    for(const auto& row : grid_)
    {
        if(spacer)
        {
            std::cout << ' ';
        }

        spacer = !spacer;

        for(const auto& tile : row)
        {
            std::cout << (int)tile << ' ';
        }
        std::cout << '\n';
    }
}

////////////////////////////////////////////////////////////////
///           protected methods                              ///
////////////////////////////////////////////////////////////////

double StratBase::EuclideanDist(size_type x1, size_type y1, size_type x2, size_type y2)
{
    return ::sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
}

StratBase::size_type StratBase::GetNeighbour(size_type x, size_type y, std::vector<vector2>& neighboor)
{
    if(x > 0)
        neighboor.push_back({x - 1, y});
    if(x + 1 < nb_col_)
        neighboor.push_back({x + 1, y});
    if(y > 0)
        neighboor.push_back({x, y - 1});
    if(y + 1 < nb_row_)
        neighboor.push_back({x, y + 1});

    if((y & 1) != 0)
    {
        if(x + 1 <= nb_col_)
        {
            if(y > 0)
                neighboor.push_back({x + 1, y - 1});
            if(y + 1 < nb_row_)
                neighboor.push_back({x + 1, y + 1});
        }
    }
    else
    {
        if(x > 0)
        {
            if(y > 0)
                neighboor.push_back({x - 1, y - 1});
            if(y + 1 < nb_row_)
                neighboor.push_back({x - 1, y + 1});
        }
    }
    return neighboor.size();
}

void StratBase::FillEmptyGrid()
{
    for(size_type i = 0; i < nb_row_; ++i)
    {
        grid_[i].reserve(nb_col_);
        for(size_type j = 0; j < nb_col_; ++j)
        {
            grid_[i].push_back(0);
        }
    }
}
