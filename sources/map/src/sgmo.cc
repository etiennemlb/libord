#include "sgmo.h"

#include <cmath>
#include <cstdlib>
#include <iostream>

////////////////////////////////////////////////////////////////
///           protected methods                              ///
////////////////////////////////////////////////////////////////


SGMO::SGMO(Regions& regions, size_type nb_row, size_type nb_col)
    : StratBase{regions, nb_row, nb_col}
{
}

void SGMO::InitRegions(std::vector<vector2>& reg_coords, const vector2& region_coords)
{
    // std::vector<vector2> neigh_vec;
    GetNeighbour(region_coords.first, region_coords.second, reg_coords);

    // reg_coords.push_back(neigh_vec[::rand() % neigh_vec.size()]);
}

void SGMO::OrganicGrowth(cell_type      region_id,
                         const vector2& from,
                         size_type      n)
{
    // std::cout << "n:" << n << " id:" << (unsigned)region_id << " x" << from.first << " y" << from.second << '\n';

    // int old                        = grid_[from.second][from.first];
    // grid_[from.second][from.first] = 7;
    // PrintGrid();
    // grid_[from.second][from.first] = old;

    if(n == 0)
    {
        return;
    }

    std::vector<vector2> neighbours;
    GetNeighbour(from.first, from.second, neighbours);

    while(!neighbours.empty())
    {
        // std::cout << neighbours.empty() << ' ' << neighbours.size() << std::endl;
        auto  it      = neighbours.begin() + (::rand() % neighbours.size());
        auto& current = *it;

        if(grid_[current.second][current.first] == 0)
        { // free cell
            grid_[current.second][current.first] = region_id;

            OrganicGrowth(region_id,
                          current,
                          n / 3);
        }
        // else if(grid_[current.second][current.first] != grid_[from.second][from.first])
        // { // encountered an other region
        //     n >>= 2;
        // }

        neighbours.erase(it); // slow
    }
}

void SGMO::EnlargeRegion(std::vector<vector2>& region_coord_list, size_type n)
{
    while(!region_coord_list.empty())
    {
        auto  it      = region_coord_list.begin() + (::rand() % region_coord_list.size());
        auto& current = *it;
        OrganicGrowth(grid_[current.second][current.first], current, n);
        region_coord_list.erase(it); // slow
    }
}