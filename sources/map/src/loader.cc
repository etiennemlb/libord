/**
 * @file loader.cc
 * @brief
 * @version 0.1
 * @date 2020-04-14
 *
      Copyright 2020 Etienne MALABOEUF, Alper OZKAN, Louis ROUSSEL

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

#include "loader.h"

SRegions* ConvertMap(Regions& regions, unsigned int& nbR, unsigned int& nbC)
{
    SRegions* reg = new SRegions;

    nbR            = 0;
    nbC            = 0;
    reg->nbRegions = regions.size();
    reg->region    = new SRegion[reg->nbRegions];
    for(unsigned int i = 0; i < reg->nbRegions; ++i)
    {
        reg->region[i].nbCells = regions[i].size();
        reg->region[i].cells   = new SRegionCell[reg->region[i].nbCells];
        for(unsigned int j = 0; j < reg->region[i].nbCells; ++j)
        {
            if(nbR < regions[i][j].first)
            {
                nbR = regions[i][j].first;
            }

            if(nbC < regions[i][j].second)
            {
                nbC = regions[i][j].second;
            }

            reg->region[i].cells[j].y = regions[i][j].first;
            reg->region[i].cells[j].x = regions[i][j].second;
        }
    }

    return reg;
}

void DeleteMap(SRegions* regions)
{
    for(unsigned int i = 0; i < regions->nbRegions; ++i)
    {
        delete[](regions->region[i].cells);
    }
    delete[] regions->region;
    delete regions;
}
